package com.kvng_willy.utilitia

import androidx.multidex.MultiDexApplication
import com.kvng_willy.utilitia.data.network.MyApi
import com.kvng_willy.utilitia.data.network.NetworkConnectionInterceptor
import com.kvng_willy.utilitia.data.preferences.PreferenceProvider
import com.kvng_willy.utilitia.data.repositories.DataRepository
import com.kvng_willy.utilitia.ui.MainViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class UtilitiaApplication:MultiDexApplication(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@UtilitiaApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { DataRepository(instance()) }
        bind() from provider { MainViewModelFactory(instance(),instance()) }
    }

}