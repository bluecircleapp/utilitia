package com.kvng_willy.utilitia.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey


class PreferenceProvider(
    context: Context
) {

    private val appContext = context.applicationContext
    private var masterKey:MasterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

    private val preference: SharedPreferences
        get() = EncryptedSharedPreferences.create(appContext,
            "secret_shared_prefs", masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )


    fun savePreference(key: String, savedAt: String?) {
        preference.edit().putString(
            key,
            savedAt
        ).apply()
    }
    fun checkPreference(key: String):Boolean{
        return preference.contains(key)
    }

    fun getLastSavedAt(key: String): String? {
        return preference.getString(key, "1")
    }

    fun deleteAll(){
        preference.edit().clear().apply()
    }

}