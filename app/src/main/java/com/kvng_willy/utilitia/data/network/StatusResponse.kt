package com.kvng_willy.utilitia.data.network

data class StatusResponse (
    val url: String?,
    val responseCode: String?,
    val responseTime: String?,
    val clasStr: String?,
    val category: String?,
    val name: String?
)