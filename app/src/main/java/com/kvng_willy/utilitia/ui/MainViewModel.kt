package com.kvng_willy.utilitia.ui

import androidx.lifecycle.ViewModel
import com.kvng_willy.utilitia.data.network.StatusResponse
import com.kvng_willy.utilitia.data.preferences.PreferenceProvider
import com.kvng_willy.utilitia.data.repositories.DataRepository
import com.kvng_willy.utilitia.utility.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.lang.Exception

class MainViewModel(
    private val prefs: PreferenceProvider,
    private val repository:DataRepository
):ViewModel() {

    var authListener: AuthListener? = null
    var statusList:ArrayList<StatusResponse> = ArrayList()

    fun fetchStatusTask(){
        val jsonResponse = repository.getStatus()
        authListener?.onTaskReturn(jsonResponse)
    }

    fun saveData(data:String?){
        //this data can be saved in room
        Coroutines.io{
            prefs.savePreference("JSON",data)
        }
    }

    suspend fun getSavedData() = withContext(Dispatchers.IO){
        try{
            val data = prefs.getLastSavedAt("JSON")
            var jsonObject = JSONObject(data!!)
            for(category in jsonObject.keys()){
                var outerObject = jsonObject.getJSONObject(category)
                for(name in outerObject.keys()){
                    var innerObject = outerObject.getJSONObject(name)
                    var statusItem = StatusResponse(
                        innerObject.optString(url),
                        innerObject.getString(code),
                        innerObject.getString(time),
                        innerObject.getString(clasStr),
                        category,
                        name
                    )
                    statusList.add(statusItem)
                }
            }
        }catch (e:Exception){
            //Error display
        }

    }
}