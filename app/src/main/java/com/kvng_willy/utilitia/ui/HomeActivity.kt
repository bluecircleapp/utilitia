package com.kvng_willy.utilitia.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.kvng_willy.utilitia.R
import com.kvng_willy.utilitia.adapter.GridAdapter
import com.kvng_willy.utilitia.data.network.StatusResponse
import com.kvng_willy.utilitia.databinding.ActivityHomeBinding
import com.kvng_willy.utilitia.utility.ClickListener
import com.kvng_willy.utilitia.utility.RecyclerTouchListener
import kotlinx.android.synthetic.main.content_home.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class HomeActivity : AppCompatActivity(),KodeinAware,AuthListener {
    override val kodein by kodein()

    private val factory: MainViewModelFactory by instance<MainViewModelFactory>()
    private lateinit var viewmodel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityHomeBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_home
        )
        setSupportActionBar(findViewById(R.id.toolbar))
        viewmodel = ViewModelProvider(this, factory).get(MainViewModel::class.java)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this
        viewmodel.authListener = this

        lifecycleScope.launch {
            viewmodel.statusList.clear()
            viewmodel.getSavedData()
            //toast(viewmodel.statusList[0].category!!)
            val mainAdapter = GridAdapter(viewmodel.statusList)
            recycler_view.apply {
                layoutManager = GridLayoutManager(context,2)
                setHasFixedSize(true)
                adapter = mainAdapter
                addOnItemTouchListener(
                    RecyclerTouchListener(
                        context,
                        recycler_view,
                        object : ClickListener {
                            override fun onClick(view: View, position: Int) {
                                val status = viewmodel.statusList[position]
                                Intent(context, DetailActivity::class.java).also {
                                    it.putExtra("Name",status.name)
                                    it.putExtra("Url",status.url)
                                    it.putExtra("Code",status.responseCode)
                                    it.putExtra("Time",status.responseTime)
                                    it.putExtra("Class",status.clasStr)
                                    startActivity(it)
                                }
                            }

                            override fun onLongClick(view: View, position: Int) {}
                        })
                )
            }
        }
    }

    override fun onTaskReturn(message: LiveData<String>?) {

    }

    override fun onTask2Return(message: ArrayList<StatusResponse>) {

    }
}